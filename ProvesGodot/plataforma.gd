extends AnimatableBody2D

@export var move_speed : float = 30.0
@export var move_dir : Vector2


var dir:String
@export var cooldown:float = 2.0
@export var vel: float =20.0


# Called when the node enters the scene tree for the first time.
func _ready():
	dir="left"

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# when we arrive at 'target_pos' switch it around.

	if cooldown>0:
		cooldown-=delta
		if dir=="left":
			position.x+=vel*delta
		elif dir=="right":
			position.x-=vel*delta
	else:
		cooldown=5.0
		if dir=="left":
			dir="right"
		elif dir=="right":
			dir="left"
	
