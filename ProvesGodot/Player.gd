extends CharacterBody2D

var move_speed : float =100.0
var jump_force : float = 200.0
var gravity : float = 500.0

var jumps : int = 2

@onready var score_text : Label = get_node("UserInterface/Score_text")

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y += gravity * delta
	else:
		jumps=2
		
	velocity.x=0
	
	#move left
	if Input.is_key_pressed(KEY_A):
		velocity.x -= move_speed
		
	if Input.is_key_pressed(KEY_D):
		velocity.x += move_speed

	if Input.is_key_pressed(KEY_SPACE) and jumps>0:
		velocity.y = -jump_force
		jumps-=1
		#print_debug(jumps)

	move_and_slide()

	if global_position.y > 100:
			game_over()

func game_over():
	get_tree().call_deferred("reload_current_scene")

func add_score(amount):
	Globals.Score+=amount
	score_text.text = str("Score: ",Globals.Score)


	
