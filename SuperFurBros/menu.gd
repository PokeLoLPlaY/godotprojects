extends Node2D

signal showMenu

@export var levelScene:PackedScene

var currentLevelScene

# Called when the node enters the scene tree for the first time.
func _ready():
	
	loadMenu();
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func loadMenu():
	$MenuCanvas/levelName.text = "SUPER FUR BROS"

func _on_button_start_pressed():
	loadLevelScene()
	$MenuCanvas.visible = false
	$HUD.visible = true
	pass # Replace with function body.



func _on_button_exit_pressed():
	_notification(NOTIFICATION_WM_CLOSE_REQUEST)
	pass # Replace with function body.



func _notification(what):
	if what == NOTIFICATION_WM_CLOSE_REQUEST:
		get_tree().quit() # default behavior

func loadLevelScene():
	#$flag.global_position = levels[index].flagCoords
	currentLevelScene = levelScene.instantiate()
	get_parent().add_child(currentLevelScene)
	pass

func deleteLevelScene():
	for child in currentLevelScene.get_children():
		child.queue_free()
	currentLevelScene.queue_free()
	pass

func _on_hud_go_back_to_menu():
	print(currentLevelScene)
	
	deleteLevelScene()
	
	$MenuCanvas.visible = true
	$HUD.visible = false
	pass # Replace with function body.


func _on_hud_restart_level():
	deleteLevelScene()
	loadLevelScene()
	pass # Replace with function body.
	


func _on_flag_level_complete():
	#print("OH MAH GAAAAHD HAS GANAO MI PANA");
	_on_hud_go_back_to_menu()
	loadMenu()
