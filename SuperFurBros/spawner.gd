extends Node2D

var RG =RandomNumberGenerator.new()
@export var vel: float =150.0
@export var platform: Array[PackedScene]
@export var flagPlatform: PackedScene

var contador = 0
@export var maxContador=3 

# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.start() # Replace with function body.
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position.x+=vel*delta


func _on_timer_timeout():
	contador += 1
	if(contador == maxContador):
		var platform_instance = flagPlatform.instantiate()
		print_debug(platform_instance.name)
		platform_instance.position = Vector2(position.x,RG.randf_range(0,-65))

		get_parent().add_child(platform_instance)
	elif contador<maxContador:
		var platform_instance = platform[RG.randi_range(0,platform.size()-1)].instantiate()
		print_debug(platform_instance.name)
		platform_instance.position = Vector2(position.x,RG.randf_range(0,-65))

		$Timer.start()
		get_parent().add_child(platform_instance)
