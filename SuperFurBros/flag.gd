extends Area2D

signal levelComplete

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_body_entered(body):
	print("flag-coll", body);
	
	if body.is_in_group("Player"):
		var menuNode = get_tree().get_root().get_node("menu")
		if(menuNode != null):
			menuNode._on_flag_level_complete()
	
	pass # Replace with function body.
