extends CharacterBody2D

@onready var _animation_player = $AnimatedSprite2D

var move_speed : float =100.0
var jump_force : float = 200.0
var gravity : float = 500.0
var hp : float = 3;

var jumps : int = 2
var canjump: bool = true
var hasjump: bool =true

func _ready():
	changeHUD()
	
func _physics_process(delta):
	
	if position.y>1000:
		getdamage(3)
	# Add the gravity.
	# Add the gravity.
	
	velocity.x=0
	
	if not is_on_floor():
		velocity.y += gravity * delta
		
		
	else:
		jumps=2
	
	
	
	#move left
	if Input.is_key_pressed(KEY_A):
		velocity.x -= move_speed
		_animation_player.flip_h=true;
		
	if Input.is_key_pressed(KEY_D):
		velocity.x += move_speed
		
		_animation_player.flip_h=false;
		

	if Input.is_physical_key_pressed(KEY_SPACE) and jumps>0 and canjump:
		velocity.y = -jump_force
		jumps-=1
		canjump=false
		$Timer.start()
		#print_debug(jumps)

	if velocity.x!=0 :
			
		_animation_player.play("MOVE")
		
	
	else:
		if velocity.y==0:
			_animation_player.play("IDLE")
		elif velocity.y>0:
			_animation_player.play("FALL")
		else: 
			_animation_player.play("JUMP")
		
	
	move_and_slide()
	
func AnimTrans(newAnim):
	_animation_player.play(newAnim)
	pass
	

func _on_timer_timeout():
	canjump=true
	pass # Replace with function body.

func getdamage(damage):
	hp-=damage
	changeHUD()
	
func changeHUD():
	
	
	var hudNode = get_tree().get_root().get_node("menu").find_child("HUD")
	
	if(hudNode != null):
		hudNode.changeHP(self.hp)
		if(self.hp<=0):
			queue_free()
			hudNode.show_buttons()
	
	pass
