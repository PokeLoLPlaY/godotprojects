extends Area2D

enum states {Wait,Attack}

var state:states =states.Wait
var target:CharacterBody2D

@export var vel: float =15.0
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(state==states.Attack && target != null):
		
		if target.position.x>global_position.x:
			position.x+=vel*delta
			
		else:
			position.x-=vel*delta


	



func _on_vision_body_entered(body):
	if(body.is_in_group("Player")):
		target=body
		state=states.Attack
		$AnimatedSprite2D.play("MOVE")





func _on_body_entered(body):
	if(body.is_in_group("Player")):
		body.getdamage(1)
		queue_free()
